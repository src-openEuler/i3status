Name:           i3status
Version:        2.14
Release:        1
Summary:        Status bar generator for i3bar, dzen2, xmobar or similar programs
License:        BSD
URL:            https://i3wm.org/i3status/
Source0:        %{url}/%{name}-%{version}.tar.xz
Source1:        %{url}/%{name}-%{version}.tar.xz.asc
# Michael Stapelberg's GPG key:
Source2:        gpgkey-424E14D703E7C6D43D9D6F364E7160ED4AC8EE1D.gpg

BuildRequires:  gcc
BuildRequires:  pkgconfig(alsa)
BuildRequires:  pkgconfig(libconfuse)
BuildRequires:  pkgconfig(libnl-genl-3.0)
BuildRequires:  pkgconfig(libpulse)
BuildRequires:  pkgconfig(yajl)
# man pages
BuildRequires:  asciidoc
BuildRequires:  xmlto
# gpg verification
BuildRequires:  gnupg2
# tests
BuildRequires:  perl
BuildRequires:  make
BuildRequires:  meson

Requires:       i3status-config

%description
i3status is a program for generating a status bar for i3bar, dzen2,
xmobar or similar programs. It issues a small number of system
calls, as one generally wants to update such status lines every
second so that the bar is updated even under load. It saves a bit of
energy by being more efficient than shell commands.

%package        config
Summary:        Upstream configuration for %{name}
BuildArch:      noarch
Requires:       %{name} = %{version}-%{release}

%description    config
This is the upstream/vanilla configuration file of %{name}.

%prep
gpg --import %{SOURCE2}
gpg --keyring %{SOURCE2} --verify %{SOURCE1}
rm -rf ~/.gnupg
%autosetup

%build
%meson
%meson_build

%install
%meson_install


%check
#check can't pass because it need sound card 
#make check -C build || (cat test-suite.log; false)

%files
%doc CHANGELOG
%license LICENSE
%{_bindir}/%{name}
%{_mandir}/man*/%{name}.1*

%files config
%config(noreplace) %{_sysconfdir}/%{name}.conf

%changelog
* Thu Feb 16 2023 lichaoran <pkwarcraft@hotmail.com> - 2.14-1
- Init package
